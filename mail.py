import smtplib, sys, datetime
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5 import uic
import time


class UI(QWidget):
    def __init__(self):
        super().__init__()
        self.InitUI()

    def InitUI(self):
        self.setGeometry(200, 200, 600, 290)

        self.setWindowTitle('Mail sender')

        self.grid = QGridLayout()
        self.grid.setSpacing(5)

        listMail = ['Google', 'Outlook', 'iCloud']

        item, ok = QInputDialog.getItem(self, 'Get Mail Server', 'Select your server for send the e-mail', listMail, 0, False)

#        self.inputLine(1, 2, 'To:')

        self.setLayout(self.grid)

        self.show()


        self.user, ok = QInputDialog.getText(self, 'Log-In', 'Log-in with your mail', QLineEdit.Normal, '')
        self.password, ok = QInputDialog.getText(self, 'Log-In', 'Put your password', QLineEdit.Password, '')

        if item == "iCloud":
            self.server = smtplib.SMTP('smtp.mail.me.com:587')

        if item == 'Google':
            self.server = smtplib.SMTP('smtp.gmail.com:587')

        if item == 'Outlook':
            self.server = smtplib.SMTP('smtp-mail.outlook.com:587')

        self.server.starttls()
        self.server.login(self.user, self.password)

#---------------------------------------------------------

        self.Line2 = QLineEdit()
        PreText = QLabel('To:')
        PreText.setAlignment(Qt.AlignRight)

        self.grid.addWidget(PreText, 2, 0)
        self.grid.addWidget(self.Line2, 2, 1)

# ---------------------------------------------------------

        self.subject = QLineEdit()
        PreText = QLabel('Subject:')
        PreText.setAlignment(Qt.AlignRight)

        self.grid.addWidget(PreText, 3, 0)
        self.grid.addWidget(self.subject, 3, 1)

# ---------------------------------------------------------

        self.Text = QTextEdit()
        self.Text.adjustSize()

        PreText = QLabel('Message:')
        PreText.setAlignment(Qt.AlignRight)

        self.grid.addWidget(PreText, 4, 0)
        self.grid.addWidget(self.Text, 4, 1, 10, 1)

        print(self.Text.toPlainText())

#---------------------------------------------------------

        sendButton = QPushButton('Send')
        self.grid.addWidget(sendButton, 12, 0)
        sendButton.clicked.connect(self.send)

#---------------------------------------------------------


        sendButton = QPushButton('Multi Send')
        self.grid.addWidget(sendButton, 13, 0)
        sendButton.clicked.connect(self.multiSend)

#---------------------------------------------------------

    @pyqtSlot()
    def send(self):

        self.date = datetime.datetime.now().strftime( "%d/%m/%Y %H:%M" )

        self.msg = "From: %s\nTo: %s\nSubject: %s\nDate: %s\n\n%s" \
                   %(self.user, self.Line2.text(), self.subject.text(), self.date, self.Text.toPlainText())

        self.server.sendmail(self.user, self.Line2.text(), self.msg)

    def multiSend(self):

        self.date = datetime.datetime.now().strftime( "%d/%m/%Y %H:%M" )

        self.msg = "From: %s\nTo: %s\nSubject: %s\nDate: %s\n\n%s" \
                   %(self.user, self.Line2.text(), self.subject.text(), self.date, self.Text.toPlainText())

        while True:
           self.server.sendmail(self.user, self.Line2.text(), self.msg)
           time.sleep(1)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = UI()
    sys.exit(app.exec_())
